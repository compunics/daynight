{% if displaycomments %}
<!--
Plugin: DayNight
Author: Michael Brüning <M.Bruening@compunics.com.ni
Version 1.4.2, tested with PTP 2.5.9 on Linux
-->
{% endif %}

{% with p.id as plugname %}
<events name="Events_{{plugname}}"  keep="true"
	{% if p.hideonhidecontrols.value %}
	hidetourcontrols="hide{{plugname}}();" 
	showtourcontrols="show{{plugname}}();" 
	{% endif %}
/>

<layer name="DayNightSwitcher" keep="true" url="{{pluginDirectory}}/switch-day.png" 
        align="{{p.ShowVariantPosition.positionString}}" x="{{p.ShowVariantPosition.x}}" y="{{p.ShowVariantPosition.y}}"
	edge="center" width="{{p.ShowVariantWidth.value}}" height="{{p.ShowVariantHeight.value}}"
	zorder="9" visible="false" alpha="0" DayNightTag="{{p.DayNightTag.value}}"
        onclick="SwitchPano_{{plugname}}();"
/>

<action name="Init_{{plugname}}" autorun="preinit">
        if( skipintro === null,
                if( tour_firstlittleplanet,
                        set( events[Events_{{plugname}}].onEndLittlePlanetIntroduction, 'CheckDayNight_{{plugname}}();' );
                );
        );
        set( events[Events_{{plugname}}].onnewpano, 'CheckDayNight_{{plugname}}();' );
	copy(action[mainloadscene_{{plugname}}].content, action[mainloadscene].content );
	txtreplace(action[mainloadscene_{{plugname}}].content, 'MERGE', 'KEEPVIEW' );
</action>

<action name="CheckDayNight_{{plugname}}">
	{% if p.ShowVariantFiles.value %}
		{% if p.ShowVariantDayFile.isRemote %}
        		set(switch_dn_img, "{{p.ShowVariantDayFile.value}}" 
		{% else %}
        		set(switch_dn_img, "%FIRSTXML%/{{pluginDirectory}}/{{p.ShowVariantDayFile.fileName}}"  );
		{% endif %}
		{% if p.ShowVariantNightFile.isRemote %}
        		set(switch_nd_img, "{{p.ShowVariantNightFile.value}}" 
		{% else %}
        		set(switch_nd_img, "%FIRSTXML%/{{pluginDirectory}}/{{p.ShowVariantNightFile.fileName}}"  );
		{% endif %}
	{% else %}
        	set(switch_dn_img, "{{dataDirectory}}/{{pluginDirectory}}/switch-day.png" );
	       	set(switch_nd_img, "{{dataDirectory}}/{{pluginDirectory}}/switch-night.png" );
	{% endif %}


	set(DAY, null);
	set(NIGHT, null);
	set(CHK, null);
	set(GID, get(scene[get(xml.scene)].group));
	set(PID, get(xml.scene) );
	txtadd(CurrentPanoTitle, get(PID), '_title' );
	getmessage(CurrentPanoName, get(CurrentPanoTitle));
	indexoftxt(DayNightTag, get(CurrentPanoName), "{{p.DayNightTag.value}}");
	if( DayNightTag GT 0,
		<!-- pano is DayNightTag-ed, find the corresponding pano -->
		set(NIGHT, get(xml.scene));
		{% if p.DayNightGroupOnly.value %}
			{% for group in project.panoGroups %}
			if( GID == {{group.tourId}},
				{% for pano in group.panos %}
					txtadd(PanoTitle, {{pano.tourId}}, '_title' );
					getmessage(tmpPano, get(PanoTitle));
					txtadd(CHK, get(tmpPano), "{{p.DayNightTag.value}}");
					if( CHK == CurrentPanoName, set(DAY, {{pano.tourId}}); );
				{% endfor %}
			);
			{% endfor %}
		{% else %}
			{% for pano in project.panoramas %}
					txtadd(PanoTitle, {{pano.tourId}}, '_title' );
					getmessage(tmpPano, get(PanoTitle));
					txtadd(CHK, get(tmpPano), "{{p.DayNightTag.value}}");
					if( CHK == CurrentPanoName, set(DAY, {{pano.tourId}}); );
			{% endfor %}
		{% endif %}

		if( DAY,
			set(layer[DayNightSwitcher].tooltip, {{p.ShowVariantDayTooltip.messageid}});
			set(layer[DayNightSwitcher].url, get(switch_dn_img) );
			set(layer[DayNightSwitcher].visible, true);
			set(layer[DayNightSwitcher].onout, hideTooltip(); );
			set(layer[DayNightSwitcher].onhover, showTooltip('plugin'); );
			tween(layer[DayNightSwitcher].alpha, 1,2);
		,
			set(layer[DayNightSwitcher].alpha,0);
			set(layer[DayNightSwitcher].visible, false);
			hideTooltip();
		);
	,
		<!-- pano is NOT DayNightTag-ged, do we have one? -->
		txtadd(CHK, get(CurrentPanoName), "{{p.DayNightTag.value}}");
		{% if p.DayNightGroupOnly.value %}
			{% for group in project.panoGroups %}
			if( GID == {{group.tourId}},
				{% for pano in group.panos %}
					txtadd(PanoTitle, {{pano.tourId}}, '_title' );
					getmessage(tmpPano, get(PanoTitle));
					if( CHK == tmpPano, set(NIGHT, {{pano.tourId}}); );
				{% endfor %}
			);
			{% endfor %}
		{% else %}
			{% for pano in project.panoramas %}
					txtadd(PanoTitle, {{pano.tourId}}, '_title' );
					getmessage(tmpPano, get(PanoTitle));
					if( CHK == tmpPano, set(NIGHT, {{pano.tourId}}); );
			{% endfor %}
		{% endif %}

		if( NIGHT,
			set(layer[DayNightSwitcher].tooltip, {{p.ShowVariantNightTooltip.messageid}});
			set(layer[DayNightSwitcher].url, get(switch_nd_img) );
			set(layer[DayNightSwitcher].visible, true);
			set(layer[DayNightSwitcher].onout, hideTooltip(); );
			set(layer[DayNightSwitcher].onhover, showTooltip('plugin'); );
			tween(layer[DayNightSwitcher].alpha, 1,2);
		,
			set(layer[DayNightSwitcher].alpha,0);
			set(layer[DayNightSwitcher].visible, false);
			hideTooltip();
		);
	);
</action>

<action name="SwitchPano_{{plugname}}">
	if( DAY, set(NEWSCENE, get(DAY)); , set(NEWSCENE, get(NIGHT)); );
	mainloadscene_{{plugname}}(get(NEWSCENE));
</action>

{% if p.hideonhidecontrols.value %}
	<action name="hide{{plugname}}">
		set(layer[DayNightSwitcher].visible, false);
	</action>
	<action name="show{{plugname}}">
		CheckDayNight_{{plugname}}();
	</action>
{% endif %}

{% endwith %}
